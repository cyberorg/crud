package net.virtadev.test.activecrud.test.app.del;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Type;

import org.junit.Test;

import com.google.gson.Gson;

import net.virtadev.test.activecrud.Router;
import net.virtadev.test.activecrud.struct.StdTxStruct;
import net.virtadev.test.activecrud.util.C;

public class DelByIdAppTest {
	private static final Gson gson = new Gson();
	private static final Type txType = new StdTxStruct().getClass();
	
	@Test
	public void testBadMagic(){
		//bad magic
		String json="{\"action\":\"delByID\",\"magic\":\"Wrong Magic\",\"data\":{\"id\":\"12121\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_MAGIC,code);
	}
	
	@Test
	//record not found
	public void testRecordNotFound(){
		//delete non-exists record
		String json="{\"action\":\"delByID\",\"magic\":\"Magic\",\"data\":{\"id\":\"12121\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.DB_ENTRY_NOT_FOUND,code);
	}
}
