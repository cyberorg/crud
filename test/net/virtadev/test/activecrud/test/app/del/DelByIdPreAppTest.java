package net.virtadev.test.activecrud.test.app.del;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Type;

import org.junit.Test;

import net.virtadev.test.activecrud.Router;
import net.virtadev.test.activecrud.struct.StdTxStruct;
import net.virtadev.test.activecrud.util.C;

import com.google.gson.Gson;

public class DelByIdPreAppTest {
	private static final Gson gson = new Gson();
	private static final Type txType = new StdTxStruct().getClass();
	
	@Test
	public void testJsonWithoutData(){
		//json with no data section
		String json = "{\"action\":\"delByID\",\"shit\":\"sdsfs\"}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_JSON,code);
	}
	
	@Test
	public void testNoIdKey(){
		//Json with wrong key
		String json = "{\"action\":\"delByID\",\"magic\":\"Magic\",\"data\":\"dgs\"}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_JSON,code);
	}
	
	@Test
	public void testEmptyID(){
		//empty ID
		String json="{\"action\":\"delByID\",\"magic\":\"Magic word here\",\"data\":{\"id\":\"\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_JSON,code);
	}
	@Test
	public void testIdNotInt(){
		//empty ID
		String json="{\"action\":\"delByID\",\"magic\":\"Magic word here\",\"data\":{\"id\":\"x\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_JSON,code);
	}
	
}
