package net.virtadev.test.activecrud.test.app.ins;

import static org.junit.Assert.*;

import java.lang.reflect.Type;

import net.virtadev.test.activecrud.Router;
import net.virtadev.test.activecrud.struct.StdTxStruct;
import net.virtadev.test.activecrud.util.C;

import org.junit.Test;

import com.google.gson.Gson;

public class InsPreAppTest {

	private static final Gson gson = new Gson();
	private static final Type txType = StdTxStruct.class;

	@Test
	public void testJsonwithoutData() {
		//send Json with empty String
		String json = "{\"action\":\"ins\",\"magic\":\"Magic\"}";
		String appOut = Router.route(json);
			
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		assertEquals(C.WRONG_JSON,code);
	}
	@Test
	public void testJsonwithoutMagic() {
		//send Json with empty String
		String json = "{\"action\":\"ins\",\"data\":{\"fn\":\"Fn\",\"ln\":\"LN\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		assertEquals(C.WRONG_JSON,code);
	}
	
	@Test
	public void testMissingKeyInJson() {
		//send Json with wrong (missing) key
		String json = "{\"action\":\"ins\",\"magic\":\"Magic\",\"data\":{\"ln\":\"LN\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		assertEquals(C.WRONG_JSON,code);
	}
	
	@Test
	public void testEmptyDataInJson() {
		//send Json with empty String
		String json = "{\"action\":\"ins\",\"magic\":\"Magic\",\"data\":{\"fn\":\"\",\"ln\":\"Schmidt\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		assertEquals(C.WRONG_DATA,code);
	}
	
	@Test
	public void testVeryLongData() {
		//send Json with empty String
		String json = "{\"action\":\"ins\",\"magic\":\"Magic\",\"data\":{\"fn\":\"felxjgvlhslbslhflhsflhslfhlshflshfolshflshlshflshflshflshflsrubvkxrubkxvktgfkwevbfkwvbfkvfksrksbcksryksdkshdkshd\",\"ln\":\"Schmidt\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		assertEquals(C.WRONG_DATA,code);
	}

}
