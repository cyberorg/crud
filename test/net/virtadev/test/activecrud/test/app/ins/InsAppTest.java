package net.virtadev.test.activecrud.test.app.ins;

import static org.junit.Assert.*;

import java.lang.reflect.Type;


import net.virtadev.test.activecrud.Router;

import net.virtadev.test.activecrud.struct.StdTxStruct;
import net.virtadev.test.activecrud.util.C;

import org.junit.Test;

import com.google.gson.Gson;


public class InsAppTest {
	
	private static final Gson gson = new Gson();
	private static final Type txType = new StdTxStruct().getClass();
	
	
	@Test
	public void testInsDuplicateEntry() {
		//record that already in base
		String json = "{\"action\":\"ins\",\"magic\":\"Magic\",\"data\":{\"fn\":\"Raimo\",\"ln\":\"Schmidt\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		
		assertEquals(C.DB_DUPLICATE_ENTRY,code);
	}
	
	@Test
	public void testWrongMagic() {
		//record that already in base
		String json = "{\"action\":\"ins\",\"magic\":\"TotallyWrong\",\"data\":{\"fn\":\"Raimo\",\"ln\":\"Schmidt\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_MAGIC,code);
	}
}
