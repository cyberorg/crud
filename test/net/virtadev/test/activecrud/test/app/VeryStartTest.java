package net.virtadev.test.activecrud.test.app;

import static org.junit.Assert.*;

import java.lang.reflect.Type;

import net.virtadev.test.activecrud.Router;
import net.virtadev.test.activecrud.struct.StdTxStruct;

import org.junit.Test;

import com.google.gson.Gson;

public class VeryStartTest {
	
	private static final Gson gson = new Gson();
	private static final Type txType = new StdTxStruct().getClass();
		
	@Test
	public void testWrongAction() {
		//send wrong Action
		String json = "{\"action\":\"Raimo\"}";
		
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		//code 10
		assertEquals(10,code);
	}
	@Test
	public void testNoAction() {
		//send no Action key
		String json = "{\"fn\":\"Raimo\"}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		//code 7
		assertEquals(7,code);
	}
	@Test
	public void testNotAJson() {
		//send no Action key
		String json = "no_json_at_all";
		String appOut  = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType); 
		int code = result.getReplyCode();
		//code 7
		assertEquals(7,code);
	}
	

}
