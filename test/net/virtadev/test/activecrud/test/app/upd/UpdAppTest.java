package net.virtadev.test.activecrud.test.app.upd;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Type;

import org.junit.Test;

import net.virtadev.test.activecrud.Router;
import net.virtadev.test.activecrud.struct.StdTxStruct;
import net.virtadev.test.activecrud.util.C;

import com.google.gson.Gson;

public class UpdAppTest {
	private static final Gson gson = new Gson();
	private static final Type txType = new StdTxStruct().getClass();
	
	@Test
	public void testRecordNotFound(){
		//trying to Update non-existing record
		String json = "{\"action\":\"upd\",\"magic\":\"Magic\",\"data\":{\"id\":\"999999\",\"fn\":\"FN\",\"ln\":\"LN\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.DB_ENTRY_NOT_FOUND,code);
	}
	public void testBadMagic(){
		//Json with bad magic
		String json = "{\"action\":\"upd\",\"magic\":\"BadMagic\",\"data\":{\"id\":\"999999\",\"fn\":\"FN\",\"ln\":\"LN\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_MAGIC,code);
	}
}
