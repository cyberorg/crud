package net.virtadev.test.activecrud.test.app.upd;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Type;

import org.junit.Test;

import net.virtadev.test.activecrud.Router;
import net.virtadev.test.activecrud.struct.StdTxStruct;
import net.virtadev.test.activecrud.util.C;

import com.google.gson.Gson;

public class UpdPreAppTest {
	private static final Gson gson = new Gson();
	private static final Type txType = new StdTxStruct().getClass();
	
	@Test
	public void testNoDataInJson() {
		//send Json with empty String
		String json = "{\"action\":\"upd\",\"magic\":\"Magic\",\"sec\":\"val\"}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_JSON,code);
	}
	
	@Test
	public void testNoKeyInJson(){
		//send JSON with wrong key
		String json = "{\"action\":\"upd\",\"magic\":\"Magic\",\"data\":{\"id\":\"999999999\",\"ln\":\"LN\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_JSON,code);
	}
	
	@Test
	public void testEmptyDataInJson() {
		//send Json with empty String
		String json = "{\"action\":\"upd\",\"magic\":\"Magic\",\"data\":{\"id\":\"999999\",\"fn\":\"\",\"ln\":\"LN\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_DATA,code);
	}
	
	@Test
	public void testDataTooLong(){
		//send JSON with wrong key
		String json = "{\"action\":\"upd\",\"magic\":\"Magic\",\"data\":{\"i\":\"9999\",\"fn\":\"VeryLongNamesdgfhglsufujsljfgfxjjsljgsljglsjglsjglsjglsgjlsjglsjglsjglsgjlsgjslgjslgjslgjslflsjflsjf\",\"ln\":\"LN\"}}";
		String appOut = Router.route(json);
		
		StdTxStruct result = gson.fromJson(appOut, txType);
		int code = result.getReplyCode();
		
		assertEquals(C.WRONG_DATA,code);
	}
	
}
