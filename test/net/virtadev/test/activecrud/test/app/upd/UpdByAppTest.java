package net.virtadev.test.activecrud.test.app.upd;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Type;

import org.junit.Test;

import net.virtadev.test.activecrud.Router;
import net.virtadev.test.activecrud.struct.UpdByTxStruct;
import net.virtadev.test.activecrud.util.C;

import com.google.gson.Gson;

public class UpdByAppTest {
	private static final Gson gson = new Gson();
	private static final Type txType = UpdByTxStruct.class;
	
	@Test
	public void testRecordNotFoundJson() {
		//send Json with empty String
		String json = "{\"action\":\"updGetDataByID\",\"data\":{\"id\":\"33323232\"}}";
				String appOut = Router.route(json);
				
				UpdByTxStruct result = gson.fromJson(appOut, txType);
				int code = result.getReplyCode();
				assertEquals(C.DB_ENTRY_NOT_FOUND,code);
		
	}
}
