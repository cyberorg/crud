package net.virtadev.test.activecrud.util;

/**
 * Class for reply Codes
 * @author asm
 *
 */
public class C {
	
	//by parser
	//x-wrong data recevied
	public final static int TRANSMITTION_LAYER_ERROR= 3;//wrong num of args
	public final static int EMPTY_JSON= 5;
	public final static int WRONG_JSON= 7;
	
	//10 - Routing (because between start and data)
	public final static int WRONG_ACTION= 10;
	
	//1x and 2x -data Error
	public final static int EMPTY_DATA= 11;
	public final static int WRONG_DATA= 12;
	public final static int WRONG_MAGIC= 13;
	
	//3x - app Internal Errors
	public final static int EMPTY_MAGIC = 30;
	public final static int MAGIC_IO = 31;
	
	//4x - DB Errors
	public final static int DB_CONNECT= 40;
	public final static int DB_MODEL= 41;
	public final static int DB_SAVE = 42;
	public final static int DB_DUPLICATE_ENTRY = 43;
	public final static int DB_ENTRY_NOT_FOUND = 44;
	public final static int DB_DELETE = 45;
	
	//5x - runtime (and uncatched)
	//general error (in case we don't know error)
	public final static int RUNTIME_ERROR= 55;
	public final static int JSON_PARSER_ERROR= 56;
	public final static int IO_ERROR=57;
	
	//debug levels
	public final static int NO = 0;
	public final static int INFO = 1;
	public final static int DEBUG = 10;
	
}
