package net.virtadev.test.activecrud.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.HashMap;

import net.virtadev.test.activecrud.exceptions.app.AppIOException;
import net.virtadev.test.activecrud.exceptions.app.DBConnectException;

import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.InitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

public class DBHelper {
	public static void connect() throws DBConnectException, AppIOException{
		HashMap<String, String> info = DBHelper.getInfo();
		try{
			Base.open("com.mysql.jdbc.Driver","jdbc:mysql://"+info.get("host")+"/"+info.get("db"),info.get("user"),info.get("pass"));
		}catch(InitException ie){
			ie.printStackTrace(System.err);
			Logger logger = LoggerFactory.getLogger(DBHelper.class);
			logger.error("Connection to: "+info.get("host")+" using username: "+info.get("user")+" failed");
			throw new DBConnectException("Cannot connect to DB");
		}catch(Exception e){
			e.printStackTrace();
			Logger logger = LoggerFactory.getLogger(DBHelper.class);
			logger.error("Connection to: "+info.get("host")+" using username: "+info.get("user")+" failed");
			throw new DBConnectException("Connection failed");
		}
	}
	public static void close(){
		if(Base.hasConnection()){
			Base.close();
		}
	}
	private static HashMap<String, String> getInfo() throws AppIOException{
		Gson gson=new Gson();
		Type type = new TypeToken<HashMap<String,String>>(){}.getType();
		HashMap<String,String> info = new HashMap<String, String>();
		try{
			FileReader fileReader = new FileReader(new File("cnf/db.json"));
			info = gson.fromJson(fileReader,type);
		}catch(FileNotFoundException fnfe){
			throw new AppIOException("File with db connect info not found");
		}catch(JsonIOException jioe){
			throw new AppIOException("Json IO Exception occurs");
		}
		
		return info;

	}
}
