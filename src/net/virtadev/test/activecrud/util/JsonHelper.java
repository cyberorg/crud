package net.virtadev.test.activecrud.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

import com.google.gson.Gson;

public class JsonHelper {
	private static final Gson gson = new Gson();
	
	public static Object fromJson(String json, Object returnObj, Type type){
		 return gson.fromJson(json,type);
	}
	public static Object fromJson(File file, Object returnObj, Type type) throws FileNotFoundException{
		FileReader fileReader = new FileReader(file);
		return gson.fromJson(fileReader,type);
	}
	
	public static String toJson(Object src){
		return gson.toJson(src);
	}
	public static void toJson(Object src,String fileName) throws IOException{
		JsonHelper.toJson(src, fileName,false);
	}
	public static void toJson(Object src, String fileName, boolean append) throws IOException{
		String json = gson.toJson(src);
		FileWriter writer = new FileWriter(fileName,append);
		writer.write(json);
		writer.close();
	}
}
