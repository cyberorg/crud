package net.virtadev.test.activecrud.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

import net.virtadev.test.activecrud.exceptions.app.AppIOException;

import com.google.gson.reflect.TypeToken;

public class S {

	private static final Type type = new TypeToken<HashMap<String,String>>(){}.getType();
	
	private static final String CONFIG = "cnf/Settings.json";
	private static final String debugKey = "debug";
	
	@SuppressWarnings("unchecked")
	public static int getDebug(){  
        try {
        	HashMap<String,String>json = new HashMap<String,String>();
        	json = (HashMap<String, String>) JsonHelper.fromJson(new File(CONFIG), json, type);
        	String debug = json.get(debugKey);
			if(debug == null){
				S.setDebug();
			} else {
				return Integer.parseInt(debug);
			}
		} catch (Exception e) {
			//if something wrong set default level
			S.setDebug();
		}
        return 0;
	}
	public static void setDebug(int level){
		try{
			//make settings hashMap
			HashMap<String,String> settings = new HashMap<String, String>();
			settings.put(debugKey,level+"");

			JsonHelper.toJson(settings, CONFIG);
		}catch(IOException e){
			AppIOException aioe = new AppIOException("Settings file not found");
			aioe.handle();
		}
	}
	
	public static void setDebug(){
		S.setDebug(C.NO);
	}
}
