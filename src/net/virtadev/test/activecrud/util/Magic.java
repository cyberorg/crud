package net.virtadev.test.activecrud.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.HashMap;

import net.virtadev.test.activecrud.exceptions.app.EmptyMagicException;
import net.virtadev.test.activecrud.exceptions.app.MagicIOException;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;

public class Magic {
	private static final String magicJSON = "cnf/magic.json";
	private static final String magicKey = "magic";
	
	public static String getMagic(){
		Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String,String>>(){}.getType();
        try {
			FileReader magicFileReader = new FileReader(new File(magicJSON));
			HashMap<String,String>json = gson.fromJson(magicFileReader,type);
			String magic = json.get(magicKey);
			if(magic == null){
				throw new EmptyMagicException();
			} else {
			return magic;
			}
		} catch (FileNotFoundException fnfe) {
			MagicIOException mioe = new MagicIOException();
			mioe.handle();
		} catch(JsonIOException jioe){
			MagicIOException mioe = new MagicIOException();
			mioe.handle();
		} catch(EmptyMagicException eme){
			eme.handle();
		}
        return "";
        
        //HashMap<String,String>jsonIn = gson.fromJson(json,type);
        
	}
}
