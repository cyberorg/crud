package net.virtadev.test.activecrud;

import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.exceptions.app.EmptyJsonException;
import net.virtadev.test.activecrud.exceptions.app.RouterException;

public class Router {
	public static String route(String json){		
		try{
		//test if json if not empty
		if(json.length()==0){
			throw new EmptyJsonException("Provided JSON is empty");
		}
		//Rx
		String action = StdRx.getAction(json);

		if(action.equals("ins")){
			return net.virtadev.test.activecrud.app.ins.Start.enter(json);
		} else if(action.equals("delByID")){
			return net.virtadev.test.activecrud.app.del.id.Start.enter(json);
		} else if(action.equals("updGetDataByID")){
			return net.virtadev.test.activecrud.app.upd.id.Start.enter(json);
		} else if(action.equals("upd")){
			return net.virtadev.test.activecrud.app.upd.Start.enter(json);
		} else if(action.equals("read")){
			return net.virtadev.test.activecrud.app.read.Start.enter(json);
		} else if(action.equals("void")){
			//stub for tests
		} else {
			throw new RouterException("We got wrong action (possible reasons: re-transmittion layer error, JavaScript modifications at your side");
		}
		}catch (AppException ae) {
			return ae.handle();
		}catch (Exception e){
			e.printStackTrace();
			return StdTx.reply(1,"General error occured");
		}
		//stub
		return StdTx.reply(0);
	}


}
