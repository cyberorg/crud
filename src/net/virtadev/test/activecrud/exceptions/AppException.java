package net.virtadev.test.activecrud.exceptions;

import net.virtadev.test.activecrud.StdTx;

@SuppressWarnings({ "serial"})
public class AppException extends Exception {
	private int code;
	private String message;
	
	protected AppException(int code,String message){
		this(code,message,true);
	}
	protected AppException(int code, String message,boolean fatal){
		this.code = code;
		this.message = message;
	}
	//TODO loggable exceptions support
	public String handle(){
		return StdTx.reply(this.code,this.message);
	}
	public String handleTest() {
		return StdTx.reply(this.code, this.message);
	}
	public int simpleTestHandler(){
		return this.code;
	}
	
	
	
}
