package net.virtadev.test.activecrud.exceptions;


@SuppressWarnings("serial")
class GeneralException extends Exception {
	
	private boolean fatal;
	/**
	 * is it need to log this exception
	 */
	private boolean loggable;
	/**
	 * log file location
	 */
	//private String log;
	
	//all variably
	protected GeneralException(boolean fatal, boolean loggable, String log){
		this.fatal=fatal;
		this.loggable=loggable;
		//this.log=log;
	}
	//all default
	protected GeneralException(){
		this.fatal=true;
		this.loggable=false;
		//this.log="";
	}
	//variably fatal - no log
	protected GeneralException(boolean fatal){
		this(fatal,false,"");
	}
	
	
	protected void handle() {
		if(this.loggable){
			//TODO log it
		}
		if(this.fatal){
			System.exit(1);
		}
	}
	//TODO handle() for log override - with file and message
	
	
	
}
