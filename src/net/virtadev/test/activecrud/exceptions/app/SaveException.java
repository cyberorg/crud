package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class SaveException extends AppException implements IException {
	public SaveException(String message) {
		super(C.DB_SAVE, message);
	}
}
