package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class RouterException extends AppException implements IException {
	public RouterException(String message){
		super(C.WRONG_ACTION,message);
	}
}
