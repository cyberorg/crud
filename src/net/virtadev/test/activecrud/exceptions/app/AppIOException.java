package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class AppIOException extends AppException implements IException {
	public AppIOException(String message) {
		super(C.IO_ERROR, message);
	}
}
