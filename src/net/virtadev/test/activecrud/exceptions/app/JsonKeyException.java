package net.virtadev.test.activecrud.exceptions.app;


import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class JsonKeyException extends AppException implements IException{
	public JsonKeyException(String key){
		super(C.WRONG_JSON,"We got malformed JSON (key: "+key+") is missing");	
	}
}
