package net.virtadev.test.activecrud.exceptions.app;


import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class EmptyJsonException extends AppException implements IException{
	public EmptyJsonException(){
		super(C.EMPTY_JSON,"Got empty JSON",true);	
	}
	public EmptyJsonException(String message){
		super(C.EMPTY_JSON,message,true);	
	}
}
