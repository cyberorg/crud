package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class DuplicateEntryException extends AppException implements IException {
	public DuplicateEntryException() {
		super(C.DB_DUPLICATE_ENTRY,"Duplicate entry found");
	}
}
