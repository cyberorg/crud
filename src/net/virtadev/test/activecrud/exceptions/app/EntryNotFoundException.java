package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class EntryNotFoundException extends AppException implements IException {
	public EntryNotFoundException() {
		super(C.DB_ENTRY_NOT_FOUND,"Entry not found");
	}
}
