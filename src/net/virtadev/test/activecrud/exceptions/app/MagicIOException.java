package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class MagicIOException extends AppException implements IException {

	public MagicIOException(String message) {
		super(C.MAGIC_IO, message);
	}
	public MagicIOException() {
		super(C.MAGIC_IO,"Something wrong with magic (magic file missed)");
	}

}
