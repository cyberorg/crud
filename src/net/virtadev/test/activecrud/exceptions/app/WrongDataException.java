package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class WrongDataException extends AppException implements IException {
	public WrongDataException(){
		super(C.WRONG_DATA,"Wrong Data Send");
	}
	public WrongDataException(String message){
		super(C.WRONG_DATA,message);
	}
}
