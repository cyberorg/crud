package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class MalformedJsonException extends AppException implements IException {
	public MalformedJsonException(){
		super(C.WRONG_JSON,"Malformed json sent");
	}
	public MalformedJsonException(String reason){
		super(C.WRONG_JSON,"Malformed json sent. Reason: "+reason);
	}

}
