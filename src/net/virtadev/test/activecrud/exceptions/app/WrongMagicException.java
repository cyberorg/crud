package net.virtadev.test.activecrud.exceptions.app;


import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class WrongMagicException extends AppException implements IException {
	public WrongMagicException(){
		super(C.WRONG_MAGIC,"Wrong Magic");
	}
	public WrongMagicException(String message){
		super(C.WRONG_MAGIC,message);
	}
}
