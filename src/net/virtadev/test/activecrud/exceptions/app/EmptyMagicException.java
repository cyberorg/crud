package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class EmptyMagicException extends AppException implements IException {
	public EmptyMagicException() {
		super(C.EMPTY_MAGIC,"Something wrong with magic (magic undefined)");
	}
	public EmptyMagicException(String message) {
		super(C.EMPTY_MAGIC, message);
	}

}
