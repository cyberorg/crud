package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class JsonParserException extends AppException implements IException {
	public JsonParserException(){
		super(C.JSON_PARSER_ERROR,"Json Parser Error");
	}
	public JsonParserException(String message){
		super(C.JSON_PARSER_ERROR,"Json Parser Error: "+message);
	}
}
