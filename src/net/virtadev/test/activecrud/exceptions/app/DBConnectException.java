package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class DBConnectException extends AppException implements IException {
	public DBConnectException(String message) {
		super(C.DB_CONNECT, message);
	}
}
