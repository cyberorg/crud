package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class AppRunTimeException extends AppException implements IException{

	protected AppRunTimeException(String message) {
		super(C.RUNTIME_ERROR, message);
	}
}
