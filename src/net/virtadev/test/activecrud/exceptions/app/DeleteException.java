package net.virtadev.test.activecrud.exceptions.app;

import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class DeleteException extends AppException implements IException {
	public DeleteException(String message) {
		super(C.DB_DELETE, message);
	}
}
