package net.virtadev.test.activecrud.exceptions.app;


import net.virtadev.test.activecrud.exceptions.IException;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.util.C;

@SuppressWarnings("serial")
public class StartException extends AppException implements IException{
	public StartException(String message){
		super(C.TRANSMITTION_LAYER_ERROR, message);	
	}
}
