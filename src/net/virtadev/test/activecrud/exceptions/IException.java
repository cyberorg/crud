package net.virtadev.test.activecrud.exceptions;

public interface IException {
	public String handle();
}
