package net.virtadev.test.activecrud;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import net.virtadev.test.activecrud.exceptions.app.MalformedJsonException;
import net.virtadev.test.activecrud.struct.RxStruct;

public class StdRx {
	private static final Gson gson = new Gson();
	
	 static String getAction(String json) throws MalformedJsonException{	
		try{
			RxStruct parsedJson = gson.fromJson(json,RxStruct.class);
			String action = parsedJson.getAction();
			if(action == null){
				throw new NullPointerException();
			}
			return action;
		}catch(JsonSyntaxException jse){
			throw new MalformedJsonException("Param must be valid JSON. See API");
		}catch (NullPointerException npe) {
			throw new MalformedJsonException("There is no action key in JSON. See API");
		}
	 }
}
