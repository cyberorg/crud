package net.virtadev.test.activecrud;

import net.virtadev.test.activecrud.struct.StdTxStruct;

import com.google.gson.Gson;

public class StdTx {
	private static final StdTxStruct sTx = new StdTxStruct();
	private static final Gson gson = new Gson();
	
	public static String reply(int code){
         return  StdTx.reply(code,"");
	}

	//For tests and not traceable exceptions
	public static String reply(int code,String message){	
		sTx.setReplyCode(code);
		sTx.setMessage(message);
	    return StdTx.reply(sTx);
	}
		
	//private 
	private static String reply(StdTxStruct sTx){		 
	      String replyStr=gson.toJson(sTx);
	      return replyStr;
	}
}
