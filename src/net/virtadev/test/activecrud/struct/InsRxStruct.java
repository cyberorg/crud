package net.virtadev.test.activecrud.struct;

public class InsRxStruct extends RxStruct implements IStruct {
	private String magic;
	private Data data;
	
	public String getMagic() {
		return magic;
	}
	public Data getData() {
		return data;
	}
	public void setMagic(String magic) {
		this.magic = magic;
	}

	public void setData(Data data) {
		this.data = data;
	}
	
	//Data innerClass
	public static class Data{

		private String fn;
		private String ln;

		public String getFn() {
			return fn;
		}
		public String getLn() {
			return ln;
		}

		public void setFn(String fn) {
			this.fn = fn;
		}
		public void setLn(String ln) {
			this.ln = ln;
		}
		

		
	}

}

