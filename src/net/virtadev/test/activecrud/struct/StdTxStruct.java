package net.virtadev.test.activecrud.struct;

public class StdTxStruct implements IStruct {
	private int replyCode;
	private String message;
	
	public int getReplyCode() {
		return replyCode;
	}
	public String getMessage() {
		return message;
	}
	public void setReplyCode(int replyCode) {
		this.replyCode = replyCode;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
