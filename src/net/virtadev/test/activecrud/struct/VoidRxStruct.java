package net.virtadev.test.activecrud.struct;

public class VoidRxStruct extends RxStruct implements IStruct {
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
