package net.virtadev.test.activecrud.struct;


public class UpdByTxStruct extends StdTxStruct implements IStruct{
	private Data data;
	
	//inner
	public static class Data{
		private String fn;
		private String ln;
		public String getFn() {
			return fn;
		}
		public String getLn() {
			return ln;
		}
		public void setFn(String fn) {
			this.fn = fn;
		}
		public void setLn(String ln) {
			this.ln = ln;
		}
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
