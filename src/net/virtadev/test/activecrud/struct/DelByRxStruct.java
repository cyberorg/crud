package net.virtadev.test.activecrud.struct;

public class DelByRxStruct extends RxStruct implements IStruct {
	private String magic;
	private Data data;
	
	public String getMagic() {
		return magic;
	}
	public Data getData() {
		return data;
	}

	public void setMagic(String magic) {
		this.magic = magic;
	}
	public void setData(Data data) {
		this.data = data;
	}
	//inner class
	public static class Data{
		private int id;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
	}
}
