package net.virtadev.test.activecrud.struct;

public class UpdByRxStruct extends RxStruct implements IStruct {
	private Data data;
	//inner
	public static class Data{
		private int id;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
}
