package net.virtadev.test.activecrud.struct;

public class RxStruct {
	protected String action;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
