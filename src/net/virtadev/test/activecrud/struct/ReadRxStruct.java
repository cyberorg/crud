package net.virtadev.test.activecrud.struct;

public class ReadRxStruct extends RxStruct implements IStruct {
	private Data data;
	
	//Data inner class
	public static class Data{
		private int limit;

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}
	}
	public Data getData(){
		return this.data;
	}
	public void setData(Data data) {
		this.data = data;
	} 
}
