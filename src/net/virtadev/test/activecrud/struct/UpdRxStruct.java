package net.virtadev.test.activecrud.struct;

public class UpdRxStruct extends RxStruct implements IStruct {
	private String magic;
	private Data data;
	
	public static class Data{
		private int id;
		private String fn;
		private String ln;
		public int getId() {
			return id;
		}
		public String getFn() {
			return fn;
		}
		public String getLn() {
			return ln;
		}
		public void setId(int id) {
			this.id = id;
		}
		public void setFn(String fn) {
			this.fn = fn;
		}
		public void setLn(String ln) {
			this.ln = ln;
		}
	}

	public String getMagic() {
		return magic;
	}

	public Data getData() {
		return data;
	}

	public void setMagic(String magic) {
		this.magic = magic;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
