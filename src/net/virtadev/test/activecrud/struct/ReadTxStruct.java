package net.virtadev.test.activecrud.struct;

import java.util.ArrayList;

public class ReadTxStruct extends StdTxStruct implements IStruct {
	
	private ArrayList<Record> data;
	
	public static class Record{
		private int id;
		private String fn;
		private String ln;
		
		public int getId() {
			return id;
		}
		public String getFn() {
			return fn;
		}
		public String getLn() {
			return ln;
		}
		public void setId(int id) {
			this.id = id;
		}
		public void setFn(String fn) {
			this.fn = fn;
		}
		public void setLn(String ln) {
			this.ln = ln;
		}
	}

	public ArrayList<Record> getData() {
		return data;
	}

	public void setData(ArrayList<Record> data) {
		this.data = data;
	}
}
