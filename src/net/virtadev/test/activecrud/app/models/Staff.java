package net.virtadev.test.activecrud.app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;
@Table("staff")
public class Staff extends Model {
	static {
		validatePresenceOf("fname","lname");
	}
}
