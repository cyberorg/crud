package net.virtadev.test.activecrud.app.del.id;

import net.virtadev.test.activecrud.StdTx;
import net.virtadev.test.activecrud.app.models.Staff;
import net.virtadev.test.activecrud.exceptions.app.AppIOException;
import net.virtadev.test.activecrud.exceptions.app.DBConnectException;
import net.virtadev.test.activecrud.exceptions.app.DeleteException;
import net.virtadev.test.activecrud.exceptions.app.EntryNotFoundException;
import net.virtadev.test.activecrud.exceptions.app.WrongMagicException;
import net.virtadev.test.activecrud.struct.DelByRxStruct;
import net.virtadev.test.activecrud.util.DBHelper;
import net.virtadev.test.activecrud.util.Magic;

public class App {
		
	public static String Main(DelByRxStruct data) throws DBConnectException, AppIOException, EntryNotFoundException, DeleteException, WrongMagicException{
	
		String realMagic = Magic.getMagic();
	
		if(data.getMagic().equals(realMagic)){

			DBHelper.connect();
			//table
			Staff record = Staff.findById(data.getData().getId());
			if(record==null){
				throw new EntryNotFoundException();
			} else {
				if(!record.delete()){
					throw new DeleteException("Unable to delete record");
				} else {
					DBHelper.close();
					return StdTx.reply(0);
				}
			}
	} else {
		throw new WrongMagicException();
	}
}
		
}
