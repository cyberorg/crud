package net.virtadev.test.activecrud.app.del.id;

import net.virtadev.test.activecrud.StdTx;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.struct.DelByRxStruct;
import net.virtadev.test.activecrud.util.DBHelper;

public class Start {
		public static String enter(String json){
			try{
				//Rx
				DelByRxStruct appStruct = Rx.read(json);
				//Data Cleaner - not used here
				//Logic
				return App.Main(appStruct);
				//Tx - unused in this app
			}catch(AppException tae){
				DBHelper.close();
				return tae.handle();
			}catch (Exception e) {
				DBHelper.close();
				return StdTx.reply(1,"General exception thrown in delete application");
			}
		}
}
