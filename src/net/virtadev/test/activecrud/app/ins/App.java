package net.virtadev.test.activecrud.app.ins;

import net.virtadev.test.activecrud.StdTx;
import net.virtadev.test.activecrud.app.models.Staff;
import net.virtadev.test.activecrud.exceptions.app.AppIOException;
import net.virtadev.test.activecrud.exceptions.app.DBConnectException;
import net.virtadev.test.activecrud.exceptions.app.DuplicateEntryException;
import net.virtadev.test.activecrud.exceptions.app.SaveException;
import net.virtadev.test.activecrud.exceptions.app.WrongMagicException;
import net.virtadev.test.activecrud.struct.InsRxStruct;
import net.virtadev.test.activecrud.util.DBHelper;
import net.virtadev.test.activecrud.util.Magic;

public class App {
		
	public static String Main(InsRxStruct data) throws 
			WrongMagicException, SaveException, DuplicateEntryException, 
			DBConnectException, AppIOException{
		String realMagic = Magic.getMagic();
		if((data.getMagic()).equals(realMagic)){
			DBHelper.connect();
			//table (new record)
			Staff staff = new Staff();
			//before insert,let's check if have same record already
			Staff thisEmployee = Staff.findFirst("fname = ? and lname = ?",data.getData().getFn(),data.getData().getLn());
			if(thisEmployee != null){throw new DuplicateEntryException();}
			//insert	
			staff.set("fname",data.getData().getFn());
			staff.set("lname",data.getData().getLn());
			if(!staff.saveIt()){					
				throw new SaveException("Unable to save record");
			} else {
				DBHelper.close();
				return StdTx.reply(0);
			}
		} else {
			throw new WrongMagicException();
		}
	}
	
	
}
