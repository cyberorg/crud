package net.virtadev.test.activecrud.app.ins;

import net.virtadev.test.activecrud.exceptions.app.JsonKeyException;
import net.virtadev.test.activecrud.exceptions.app.JsonParserException;
import net.virtadev.test.activecrud.exceptions.app.MalformedJsonException;
import net.virtadev.test.activecrud.struct.InsRxStruct;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;


public class Rx {
	private static final Gson gson = new Gson();
	
	public static InsRxStruct read(String json) throws MalformedJsonException, JsonParserException, JsonKeyException{		
		try{
             InsRxStruct jsonIn = gson.fromJson(json,InsRxStruct.class);
             if(!checkStruct(jsonIn)){
            	 throw new MalformedJsonException("We got JSON in wrong format. Please see API.");
             }
             return jsonIn;		
			}catch(JsonSyntaxException jse){
				throw new MalformedJsonException("Wrong syntax in JSON");
			}catch(JsonParseException jpe){
				throw new JsonParserException("during inserting");
			}
	}
	static boolean checkStruct(InsRxStruct struct) throws JsonKeyException{
		try{
			//magic 
			assert(struct.getMagic() != null): "magic";
			if(struct.getMagic() == null){
				throw new JsonKeyException("magic");
			}
			//data
			assert(struct.getMagic() != null): "data";
			InsRxStruct.Data data = struct.getData();
			if(struct.getData() == null){
				throw new JsonKeyException("magic");
			}
		
			//fn
			assert(data.getFn() != null): "fn";
			if(struct.getData().getFn() == null){
				throw new JsonKeyException("fn");
			}
			//ln
			assert(data.getLn() != null): "ln";
			if(struct.getData().getLn() == null){
				throw new JsonKeyException("ln");
			}
			return true;
		}catch(NullPointerException npe){
			String msg = "void";
			throw new JsonKeyException(msg);
		}catch(AssertionError ae){
			String msg = ae.getMessage();
			throw new JsonKeyException(msg);
		}
	}
}
