package net.virtadev.test.activecrud.app.read;

import java.util.ArrayList;

import org.javalite.activejdbc.LazyList;

import net.virtadev.test.activecrud.app.models.Staff;
import net.virtadev.test.activecrud.exceptions.app.AppIOException;
import net.virtadev.test.activecrud.exceptions.app.DBConnectException;
import net.virtadev.test.activecrud.exceptions.app.EntryNotFoundException;
import net.virtadev.test.activecrud.struct.ReadRxStruct;
import net.virtadev.test.activecrud.struct.ReadTxStruct;
import net.virtadev.test.activecrud.struct.ReadTxStruct.Record;
import net.virtadev.test.activecrud.util.DBHelper;

public class App {

	public static String Main(ReadRxStruct data) throws DBConnectException, AppIOException, EntryNotFoundException{
		
		DBHelper.connect();
		//init block
		int limit=data.getData().getLimit();
		LazyList<Staff> allRecords = null;
		
		if(limit!=0){
			allRecords = Staff.findAll().limit(limit);
		} else {
			//all records
			allRecords = Staff.findAll();
		}
		//make array list
		ArrayList<Record> dataList = new ArrayList<Record>();
		
		//iterate all record,make Record objects and put them to dataList
		for(Staff staff : allRecords){
			int id = staff.getInteger("id");
			String fn = staff.getString("fname");
			String ln = staff.getString("lname");
			//make record object
			ReadTxStruct.Record record = new ReadTxStruct.Record();
			record.setId(id);
			record.setFn(fn);
			record.setLn(ln);
			//add to dataList
			dataList.add(record);
		}
				
		DBHelper.close();
		
		return Tx.talk(dataList);
	}
	
}
