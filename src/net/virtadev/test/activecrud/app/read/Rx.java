package net.virtadev.test.activecrud.app.read;

import net.virtadev.test.activecrud.exceptions.app.JsonKeyException;
import net.virtadev.test.activecrud.exceptions.app.JsonParserException;
import net.virtadev.test.activecrud.exceptions.app.MalformedJsonException;
import net.virtadev.test.activecrud.struct.ReadRxStruct;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;


public class Rx {
	private static final Gson gson = new Gson();
	
	public static ReadRxStruct read(String json) throws MalformedJsonException, JsonParserException, JsonKeyException{		
		try{
			ReadRxStruct jsonIn = gson.fromJson(json,ReadRxStruct.class);
             if(!checkStruct(jsonIn)){
            	 throw new MalformedJsonException("We got JSON in wrong format. Please see API.");
             }
             return jsonIn;		
			}catch(JsonSyntaxException jse){
				throw new MalformedJsonException("Wrong syntax in JSON");
			}catch(JsonParseException jpe){
				throw new JsonParserException("during inserting");
			}
	}
	static boolean checkStruct(ReadRxStruct struct) throws JsonKeyException{
		try{
			//data
			ReadRxStruct.Data data = struct.getData();
			assert(data != null): "data";
			
			//id
			Integer limit = new Integer(data.getLimit());
			assert (limit != null): "limit";
			
			return true;
		}catch(NullPointerException npe){
			String message = "void";
			throw new JsonKeyException(message);
		}catch (AssertionError ae) {
			String message = ae.getMessage();
			throw new JsonKeyException(message);
		}
	}
}
