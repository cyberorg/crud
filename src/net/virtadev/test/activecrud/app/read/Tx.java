package net.virtadev.test.activecrud.app.read;

import java.util.ArrayList;

import net.virtadev.test.activecrud.struct.ReadTxStruct;
import net.virtadev.test.activecrud.struct.ReadTxStruct.Record;

import com.google.gson.Gson;

public class Tx {
	private static final ReadTxStruct sTx = new ReadTxStruct();
	private static final Gson gson = new Gson();
	
	public static String talk (ArrayList<Record> dataList){
		
		sTx.setReplyCode(0);
		sTx.setMessage("Sending data");
		//set dataList
		sTx.setData(dataList);

  	   String replyStr=gson.toJson(sTx);
       return replyStr;
	}
}
