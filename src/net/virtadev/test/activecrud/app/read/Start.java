package net.virtadev.test.activecrud.app.read;

import net.virtadev.test.activecrud.StdTx;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.struct.ReadRxStruct;
import net.virtadev.test.activecrud.util.DBHelper;


public class Start {
		public static String enter(String json){
			try{
				//Rx
				ReadRxStruct appStruct = Rx.read(json);
				//Data Cleaner
				DC.filter(appStruct);
				//Logic
				return App.Main(appStruct);
				//Tx launches from App
				
			}catch(AppException tae){
				DBHelper.close();
				return tae.handle();
			}catch (Exception e) {
				DBHelper.close();
				e.printStackTrace();
				return StdTx.reply(1,"General exception thrown in update application while getting data");
			}
		}
}
