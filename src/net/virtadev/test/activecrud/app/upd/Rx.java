package net.virtadev.test.activecrud.app.upd;

import net.virtadev.test.activecrud.exceptions.app.JsonKeyException;
import net.virtadev.test.activecrud.exceptions.app.JsonParserException;
import net.virtadev.test.activecrud.exceptions.app.MalformedJsonException;
import net.virtadev.test.activecrud.struct.UpdRxStruct;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;


public class Rx {
	private static final Gson gson = new Gson();
	
	public static UpdRxStruct read(String json) throws MalformedJsonException, JsonParserException, JsonKeyException{		
		try{
             UpdRxStruct jsonIn = gson.fromJson(json,UpdRxStruct.class);
             if(!checkStruct(jsonIn)){
            	 throw new MalformedJsonException("We got JSON in wrong format. Please see API.");
             }
             return jsonIn;		
			}catch(JsonSyntaxException jse){
				throw new MalformedJsonException("Wrong syntax in JSON");
			}catch(JsonParseException jpe){
				throw new JsonParserException("during inserting");
			}
	}
	static boolean checkStruct(UpdRxStruct struct) throws JsonKeyException{
		try{
			//magic
			assert (struct.getMagic() != null) : "magic";
			
			//data
			UpdRxStruct.Data data = struct.getData();
			assert (data != null) : "data";
			
			//id
			Integer id = new Integer(data.getId());
			assert (id != null) : "id";
				
			//fn
			assert (data.getFn() != null): "fn";
	
			//ln
			assert(data.getLn() != null): "ln";
			
			return true;
			
		}catch(NullPointerException npe){
			String message = "void";
			throw new JsonKeyException(message);
		}catch(AssertionError ae){
			String message = ae.getMessage();
			throw new JsonKeyException(message);
		}
	}
}
