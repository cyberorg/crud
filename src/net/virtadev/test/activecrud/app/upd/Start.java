package net.virtadev.test.activecrud.app.upd;

import net.virtadev.test.activecrud.StdTx;
import net.virtadev.test.activecrud.exceptions.AppException;
import net.virtadev.test.activecrud.struct.UpdRxStruct;
import net.virtadev.test.activecrud.util.DBHelper;

public class Start {
		public static String enter(String json){
			try{
				//Rx
				UpdRxStruct appStruct = Rx.read(json);
				//Data Cleaner
				DC.filter(appStruct);
				//Logic
				return App.Main(appStruct);
				//Tx - unused in this app
			}catch(AppException tae){
				DBHelper.close();
				return tae.handle();
			}catch (Exception e) {
				DBHelper.close();
				e.printStackTrace();
				return StdTx.reply(1,"General exception thrown in update application");
			}
		}
}
