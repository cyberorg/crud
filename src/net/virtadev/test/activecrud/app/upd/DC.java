package net.virtadev.test.activecrud.app.upd;

import net.virtadev.test.activecrud.exceptions.app.JsonKeyException;
import net.virtadev.test.activecrud.exceptions.app.WrongDataException;
import net.virtadev.test.activecrud.struct.UpdRxStruct;
import net.virtadev.test.activecrud.util.FilterHelper;


public class DC {

	public static void filter(UpdRxStruct data) throws WrongDataException, JsonKeyException{
		//fn
		String fn = data.getData().getFn();
		if(fn == null){
			throw new JsonKeyException("fn");
		}
		if(!FilterHelper.MinMax(0, 45,fn)){
			throw new WrongDataException("FirstName lenght cannot exceed 45 letters");
		}
		//TODO RegExp test
		
		//ln
		String ln = data.getData().getLn();
		if(ln == null){
			throw new JsonKeyException("ln");
		}
		if(!FilterHelper.MinMax(0, 45,ln)){
			throw new WrongDataException("LastName lenght cannot exceed 45 letters");
		}
		//TODO RegExp test
			
	}
}
