package net.virtadev.test.activecrud.app.upd;

import net.virtadev.test.activecrud.StdTx;
import net.virtadev.test.activecrud.app.models.Staff;
import net.virtadev.test.activecrud.exceptions.app.AppIOException;
import net.virtadev.test.activecrud.exceptions.app.DBConnectException;
import net.virtadev.test.activecrud.exceptions.app.EntryNotFoundException;
import net.virtadev.test.activecrud.exceptions.app.SaveException;
import net.virtadev.test.activecrud.exceptions.app.WrongMagicException;
import net.virtadev.test.activecrud.struct.UpdRxStruct;
import net.virtadev.test.activecrud.util.DBHelper;
import net.virtadev.test.activecrud.util.Magic;

public class App {
	
	
	public static String Main(UpdRxStruct data) throws DBConnectException, AppIOException, EntryNotFoundException, SaveException, WrongMagicException{
		String realMagic = Magic.getMagic();
		if((data.getMagic()).equals(realMagic)){

			DBHelper.connect();
			//before insert,let's check if have same record already
			Staff stf = Staff.findById(data.getData().getId());
			if(stf == null){
				throw new EntryNotFoundException();
			}
			//update	
			stf.set("fname",data.getData().getFn());
			stf.set("lname",data.getData().getLn());
			if(!stf.saveIt()){
				throw new SaveException("Unable to update record");
			} else {
				DBHelper.close();
				return StdTx.reply(0);
				}
		} else {
			throw new WrongMagicException();
		}
	}
	
	
}
