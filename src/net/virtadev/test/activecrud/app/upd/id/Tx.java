package net.virtadev.test.activecrud.app.upd.id;

import com.google.gson.Gson;

import net.virtadev.test.activecrud.struct.UpdByTxStruct;

public class Tx {
	private static final UpdByTxStruct sTx = new UpdByTxStruct();
	private static final Gson gson = new Gson();
	
	public static String talk (String fn,String ln){
		  sTx.setReplyCode(0);
		  sTx.setMessage("");
		  //make data
		  UpdByTxStruct.Data data = new UpdByTxStruct.Data();
		  data.setFn(fn);
		  data.setLn(ln);
		  sTx.setData(data);
		  
		  String replyStr=gson.toJson(sTx);
	      return replyStr;
	}
}
