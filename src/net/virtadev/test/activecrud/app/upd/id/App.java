package net.virtadev.test.activecrud.app.upd.id;

import net.virtadev.test.activecrud.app.models.Staff;
import net.virtadev.test.activecrud.exceptions.app.AppIOException;
import net.virtadev.test.activecrud.exceptions.app.DBConnectException;
import net.virtadev.test.activecrud.exceptions.app.EntryNotFoundException;
import net.virtadev.test.activecrud.struct.UpdByRxStruct;
import net.virtadev.test.activecrud.util.DBHelper;
public class App {

	public static String Main(UpdByRxStruct data) throws DBConnectException, AppIOException, EntryNotFoundException{
		
		DBHelper.connect();
		Staff entry = Staff.findById(data.getData().getId());
		if(entry==null){
			throw new EntryNotFoundException();
		}
		String fn = entry.getString("fname");
		String ln = entry.getString("lname");
		DBHelper.close();
		return Tx.talk(fn, ln);

	}
	
}
